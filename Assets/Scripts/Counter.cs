﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class Counter : MonoBehaviour {

	[SerializeField] Text text = null;

	public UnityAction OnCounter;

	void OnEnable() {
		
		StartCoroutine(CountCoroutine());
	}

	IEnumerator CountCoroutine() {

		for (int i = 3; i > 0; i --) {

			text.text = i.ToString();
			yield return new WaitForSeconds(1.0f);
		}

		OnCounter?.Invoke();
	}
}
