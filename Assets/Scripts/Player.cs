﻿using UnityEngine;
using UnityEngine.Events;


public enum PlayerEventType { Arrived, Lost }

public class Player : MonoBehaviour {

	[SerializeField] float speed = 3.0f;
	[SerializeField] float slowSpeed = 1.0f;

	public Vector3[] Line { private get; set; }
	public UnityAction<PlayerEventType> OnEvent;

	int destinationIndex;
	Vector3 initialPos;

	void Awake() {
	
		initialPos = transform.position;
	}

	void OnEnable() {
	
		transform.position = Line[0];
		destinationIndex = 1;
	}

	void OnDisable() {
	
		transform.position = initialPos;
	}

	void Update() {

		float speed = Slow ? slowSpeed : this.speed;
		float translation = speed * Time.deltaTime;
		while (true) {

			Vector3 curPos = transform.position;
			Vector3 nextPos = Line[destinationIndex];

			float nextPointDistance = Vector3.Distance(nextPos, curPos);
			if (nextPointDistance > translation) {

				transform.position = Vector3.MoveTowards(curPos, nextPos, translation);
				return;
			}

			transform.position = nextPos;
			translation -= nextPointDistance;
			if (++ destinationIndex == Line.Length) {

				OnEvent?.Invoke(PlayerEventType.Arrived);
				return;
			}
		}
	}

	private void OnTriggerEnter(Collider other) {
	
		OnEvent?.Invoke(PlayerEventType.Lost);
	}

	public bool Slow { private get; set; }
}
