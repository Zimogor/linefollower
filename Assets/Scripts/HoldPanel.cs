﻿using UnityEngine;


public class HoldPanel : MonoBehaviour {

	[SerializeField] SpriteRenderer sr = null;

	void OnMouseDown() {
	
		sr.enabled = true;
	}

	void OnMouseUp() {
	
		sr.enabled = false;
	}

	void OnEnable() {
	
		sr.enabled = false;
	}

	public bool Pressed => sr.enabled;
}
