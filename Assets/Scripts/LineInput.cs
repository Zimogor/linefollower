﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;


public class LineInput : MonoBehaviour {

	[SerializeField] float segmentSize = 0.1f;
	[SerializeField] BoxCollider col = null;
	[SerializeField] LineDrawer lineDrawer = null;

	public UnityAction<Vector3[]> OnLineReady;

	List<Vector3> points = new List<Vector3>();
	Camera cam;
	bool dragging = false;
	LayerMask obstacleMask;

	void Awake() {

		obstacleMask = LayerMask.GetMask("Obstacle");
		cam = Camera.main;
	}

	void OnEnable() {
	
		col.enabled = true;
	}

	void OnDisable() {
	
		col.enabled = false;
		dragging = false;
		points.Clear();
	}

	void OnMouseDown() {

		dragging = false;
		points.Clear();

		var ray = cam.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out RaycastHit hitInfo)) {

			Vector3 point = hitInfo.point;
			if (point.z <= -3.5) {

				dragging = true;
				UpdatePointsWithInput(point);
			}
		}
	}

	void OnMouseDrag() {

		if (dragging) {

			var ray = cam.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray, out RaycastHit hitInfo)) {

				Vector3 point = hitInfo.point;
				UpdatePointsWithInput(point);
				if (points.Count > 0 && points[points.Count - 1].z >= 3.5) {
					dragging = false;
					OnLineReady?.Invoke(LineSmoother.SmoothLine(points, segmentSize));
				}
			}
		}
	}

	void UpdatePointsWithInput(Vector3 point) {

		if (points.Count == 0 && CanPlacePoint(point)) {
			points.Add(point);
			return;
		}

		if (points.Count == 1 && CanPlaceLine(points[0], point)) {
			points.Add(point);
			return;
		}

		Vector3 preLastPoint = points[points.Count - 2];
		Vector3 lastPoint = points[points.Count - 1];

		if (Vector3.Distance(lastPoint, preLastPoint) <= 0.5f && CanPlaceLine(preLastPoint, point)) {
			points[points.Count - 1] = point;
			return;
		}

		if (CanPlaceLine(lastPoint, point))
			points.Add(point);
	}

	bool CanPlaceLine(Vector3 point1, Vector3 point2) {

		return !Physics.Linecast(point1, point2, obstacleMask);
	}

	bool CanPlacePoint(Vector3 point) {

		return !Physics.Linecast(new Vector3(point.x, -0.3f, point.z), new Vector3(point.x, 0.3f, point.z), obstacleMask);
	}

	void Update() {

		if (points.Count > 0) {

			var pps = LineSmoother.SmoothLine(points, segmentSize);
			lineDrawer.SetLine(pps);

		} else {

			lineDrawer.SetLine(null);
		}
	}
}
