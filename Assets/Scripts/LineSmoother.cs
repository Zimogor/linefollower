using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineSmoother : MonoBehaviour 
{
	static AnimationCurve curveXcache = new AnimationCurve();
	static AnimationCurve curveZcache = new AnimationCurve();

	public static Vector3[] SmoothLine( List<Vector3> inputPoints, float segmentSize )
	{
		//create keyframe sets
		Keyframe[] keysX = new Keyframe[inputPoints.Count];
		Keyframe[] keysZ = new Keyframe[inputPoints.Count];

		//set keyframes
		for( int i = 0; i < inputPoints.Count; i++ )
		{
			keysX[i] = new Keyframe( i, inputPoints[i].x );
			keysZ[i] = new Keyframe( i, inputPoints[i].z );
		}

		//apply keyframes to curves
		curveXcache.keys = keysX;
		curveZcache.keys = keysZ;

		//smooth curve tangents
		for( int i = 0; i < inputPoints.Count; i++ )
		{
			curveXcache.SmoothTangents( i, 0 );
			curveZcache.SmoothTangents( i, 0 );
		}

		//list to write smoothed values to
		List<Vector3> lineSegments = new List<Vector3>();

		//find segments in each section
		for( int i = 0; i < inputPoints.Count; i++ )
		{
			//add first point
			lineSegments.Add( inputPoints[i] );

			//make sure within range of array
			if( i+1 < inputPoints.Count )
			{
				//find distance to next point
				float distanceToNext = Vector3.Distance(inputPoints[i], inputPoints[i+1]);

				//number of segments
				int segments = (int)(distanceToNext / segmentSize);

				//add segments
				for( int s = 1; s < segments; s++ )
				{
					//interpolated time on curve
					float time = ((float)s/(float)segments) + (float)i;

					//sample curves to find smoothed position
					Vector3 newSegment = new Vector3( curveXcache.Evaluate(time), (inputPoints[i].y + inputPoints[i+1].y) * 0.5f, curveZcache.Evaluate(time) );

					//add to list
					lineSegments.Add( newSegment );
				}
			}
		}

		return lineSegments.ToArray();
	}

}
