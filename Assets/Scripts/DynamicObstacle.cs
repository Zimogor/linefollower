﻿using UnityEngine;


public class DynamicObstacle : MonoBehaviour {

	[SerializeField] float speed = 1.0f;
	[SerializeField] Transform destination = null;

	Vector3 endPosition;
	Vector3 startPosition;

	void Awake() {
	
		startPosition = transform.position;
		endPosition = destination.position;
	}

	void OnDrawGizmos() {

		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, destination.position);
	}

	void Update() {
	
		transform.position = Vector3.MoveTowards(transform.position, endPosition, Time.deltaTime * speed);
		if (transform.position == endPosition) {

			Vector3 temp = startPosition;
			startPosition = endPosition;
			endPosition = temp;
		}
	}
}
