﻿using UnityEngine;


public class LineDrawer : MonoBehaviour {

	[SerializeField] LineRenderer lineRenderer = null;

	public void SetLine(Vector3[] line) {

		if (line == null) {

			lineRenderer.positionCount = 0;

		} else {

			lineRenderer.positionCount = line.Length;
			lineRenderer.SetPositions(line);
		}
	}
}
