﻿using UnityEngine;


public class GameManager : MonoBehaviour {

	enum GameState { Drawing, Counter, Game }

	[SerializeField] LineInput lineInput = null;
	[SerializeField] Counter counter = null;
	[SerializeField] Player player = null;
	[SerializeField] HoldPanel holdPanel = null;

	Vector3[] line;

	void Awake() {
	
		lineInput.OnLineReady += OnLineReady;
		counter.OnCounter += OnCounter;
		player.OnEvent += OnPlayerArrived;
	}

	void OnPlayerArrived(PlayerEventType eventType) {

		SetGameState(GameState.Drawing);
	}

	void OnLineReady(Vector3[] line) {

		this.line = line;
		SetGameState(GameState.Counter);
	}

	void OnCounter() {

		SetGameState(GameState.Game);
	}

	void Start() {
	
		lineInput.enabled = false;
		SetGameState(GameState.Drawing);
	}

	void Update() {
	
		player.Slow = holdPanel.Pressed;
	}

	void SetGameState(GameState state) {

		switch (state) {

			case GameState.Drawing:
				lineInput.enabled = true;
				counter.gameObject.SetActive(false);
				player.enabled = false;
				holdPanel.gameObject.SetActive(false);
				line = null;
				break;
			case GameState.Counter:
				lineInput.enabled = false;
				counter.gameObject.SetActive(true);
				player.enabled = false;
				holdPanel.gameObject.SetActive(true);
				break;
			case GameState.Game:
				lineInput.enabled = false;
				counter.gameObject.SetActive(false);
				player.Line = line;
				player.enabled = true;
				holdPanel.gameObject.SetActive(true);
				break;
			default:
				throw new System.NotImplementedException();
		}
	}
}
